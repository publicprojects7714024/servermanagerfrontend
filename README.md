
## Server Manager

Installation

- In the terminal, navigate to root directory
- Run `npm install`
- Execute `ng serve --open`
- It will start the app (http://localhost:4200/)
- You will be getting, the server listing page where you can filer and compare the list.
- Make sure your Server Manger back end is up and running.

## Features Available
- Server listing and filtering
- Comparing servers

## Notes
- If there any issue in running the app, make sure all the file's lines ending is in LF. If not, you can change this to LF using VS code editor (right bottom corner of the VS code interface)
