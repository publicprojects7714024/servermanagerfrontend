export interface Server {
    id: number;
    model: string;
    ram: string;
    hdd: string;
    location: string;
    price: string;
  }