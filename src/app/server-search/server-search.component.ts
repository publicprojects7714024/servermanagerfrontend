import { Component, Output, EventEmitter } from '@angular/core';
import { MatSliderModule } from '@angular/material/slider';
import { ServerFilterService } from '../server.filter.service';
import { ServerFilterResponse } from '../serverFilterResponse';

/**
 * @title Basic slider
 */
@Component({
  selector: 'app-server-search',
  templateUrl: './server-search.component.html',
  styleUrls: ['./server-search.component.css']
})
export class ServerSearchComponent {
  @Output() sliderStartValueChanged = new EventEmitter<number>();
  @Output() sliderEndValueChanged = new EventEmitter<number>();
  @Output() hddTypeValueChanged = new EventEmitter<string>();
  @Output() checkboxValueChanged = new EventEmitter<string[]>();
  @Output() locationValueChanged = new EventEmitter<string>();
  @Output() searchButtonClicked = new EventEmitter<string>();
  @Output() compareButtonClicked = new EventEmitter<string>();
  value = 0;
  max = 5000;
  min = 0;
  showTicks = false;
  step = 500;
  hddType = '';
  selectedValues: string[] = [];
  location = '';

  filters: ServerFilterResponse[] = [];
  availableHddTypes: string[] = [];
  availableRams: string[] = [];
  availableLocations: string[] = [];

  constructor(private serverFilterService: ServerFilterService) { }

  ngOnInit(): void {
    this.serverFilterService.getServerFilters()
      .subscribe((serverFilters) => {
        console.log(serverFilters)
        this.availableHddTypes = serverFilters.data.hddType;
        this.availableRams = serverFilters.data.ram;
        this.availableLocations = serverFilters.data.location;
        this.min = serverFilters.data.storage[0];
        this.max = serverFilters.data.storage[serverFilters.data.storage.length - 1];
      });
  }

  onSliderStartChange() {
    this.sliderStartValueChanged.emit(this.value);
  }

  onSliderEndChange() {
    this.sliderEndValueChanged.emit(this.value);
  }
  onHddTypeChange() {
    this.hddTypeValueChanged.emit(this.hddType);
  }
  onLocationChange() {
    this.locationValueChanged.emit(this.location);
  }
  formatLabel(value: number): string {
    if (value >= 1024) {
      return Math.round(value / 1024) + 'TB';
    }
    return `${value}GB`;
  }
  checkboxChanged(event: any): void {
    const value = event.source.value;
    const isChecked = event.checked;
    if (isChecked && !this.selectedValues.includes(value)) {
      this.selectedValues.push(value);
    } else if (!isChecked && this.selectedValues.includes(value)) {
      const index = this.selectedValues.indexOf(value);
      this.selectedValues.splice(index, 1);
    }
    this.checkboxValueChanged.emit(this.selectedValues);
  }

  compareServers() {
    this.compareButtonClicked.emit('compare');
  }

  searchServers() {
    this.searchButtonClicked.emit('search');
  }

}
