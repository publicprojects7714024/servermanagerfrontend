import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Server } from './server';
import { ServerFilter } from './serverFilter';
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class ServerService {

  private serversUrl = 'http://127.0.0.1:8000/api/servers';  // URL to web api


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

  /** GET servers from the server */
  getServers(): Observable<Server[]> {
    return this.http.get<Server[]>(this.serversUrl)
      .pipe(
        tap(_ => this.log('fetched servers')),
        catchError(this.handleError<Server[]>('getServers', []))
      );
  }

   /** GET servers from the server */
   compareServer(serverIds): Observable<Server[]> {
    let Params = new HttpParams();
    serverIds.forEach(function(value) {
      Params = Params.append('id[]', value);
    });

    return this.http.get<Server[]>(`${this.serversUrl}/compare`, {params: Params})
      .pipe(
        tap(_ => this.log('fetched servers')),
        catchError(this.handleError<Server[]>('getServers', []))
      );
  }

  /* GET servers whose name contains search term */
  searchServers(filter: ServerFilter): Observable<Server[]> {
    if (!filter) {
      // if not search term, return empty server array.
      return of([]);
    }
    console.log(filter);
    let Params = new HttpParams();
  
    Params = Params.append('hddType', filter.hddType);
    Params = Params.append('location', filter.location);
    filter.ram.forEach(function(value) {
      Params = Params.append('ram[]', value);
    });
    filter.storage.forEach(function(value) {
      Params = Params.append('storage[]', value);
    })
    return this.http.get<Server[]>(this.serversUrl, {params: Params});
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   *
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a HeroService message with the MessageService */
  private log(message: string) {

  }
}