import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ServersComponent } from './servers/servers.component';
import { ServerCompareComponent } from './server-compare/server-compare/server-compare.component';

const routes: Routes = [
  { path: '', component: ServersComponent },
  { path: 'server-compare', component: ServerCompareComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
