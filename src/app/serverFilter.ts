export interface ServerFilter {
  storage: string[];
  ram: string[];
  hddType: string;
  hdd: string;
  location: string;
}