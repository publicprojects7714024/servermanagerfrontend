import { Component } from '@angular/core';
import { Server } from 'src/app/server';
import { ServerService } from 'src/app/server.service';
import { TitleCasePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-server-compare',
  templateUrl: './server-compare.component.html',
  styleUrls: ['./server-compare.component.css']
})
export class ServerCompareComponent {
  displayedColumns: string[] = [];
  columnsToDisplay: string[] = this.displayedColumns.slice();
  data: Server[] = [];
  constructor(private serverService: ServerService, private router:Router, private route:ActivatedRoute, private titlecasePipe:TitleCasePipe) { }
  ngOnInit(): void {
    const id = (this.route.snapshot);
    const param = this.route.snapshot.queryParamMap;
    // console.log(this.router.getCurrentNavigation().extras.state);
    console.log();

    let serverIds = param.getAll('serverIds');

    this.serverService.compareServer(serverIds)
      .subscribe((apiServers) => {
        this.compare(apiServers);
      });
  }
  compare(apiServers) {

    this.data = apiServers;
    let servers = this.data;
    this.displayedColumns.push(' ');
    let server = servers[0];
    let features = Object.keys(server);
    this.data.forEach((server) => {
      this.displayedColumns.push(`${server.model} (Id: ${server.id})`);
    });
    this.columnsToDisplay = this.displayedColumns.slice();
    let newData = [];
    features.forEach((feature) => {
      if (feature == 'id' || feature == 'model') {
        return;
      }
     
      var tmp = { " ": this.titlecasePipe.transform(feature) }
      this.data.forEach((server) => {
        tmp[`${server.model} (Id: ${server.id})`] = server[feature];

      })
      newData.push(tmp);
    });
    this.data = newData;
  }
}
