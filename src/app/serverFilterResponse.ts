export interface ServerFilterResponse {
  data: {
    storage: number[],
    ram: string[],
    hddType: string[],
    location: string[],
  },
  links: {
    self: string
  }
}