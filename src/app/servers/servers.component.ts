import { Component } from '@angular/core';
import { Server } from '../server';
import { ServerService } from '../server.service';
import { ServerFilter } from '../serverFilter';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';

@Component({
  selector: 'app-servers',
  templateUrl: './servers.component.html',
  styleUrls: ['./servers.component.css'],
})
export class ServersComponent {
  sliderStartValue: number = 0;
  sliderEndValue: number = 0;
  hddType = '';
  servers: Server[] = [];
  displayedColumns: string[] = ['select', 'id', 'model', 'ram', 'hdd', 'location', 'price'];
  selection = new SelectionModel<Server>(true, []);
  ram: string[] = [];
  location: string = '';
  
  constructor(private serverService: ServerService, private router: Router) { }
  ngOnInit(): void {
    this.getServers();
  }
  getServers(): void {
    this.serverService.getServers()
      .subscribe(servers => this.servers = servers);
  }
  onSliderStartValueChanged(value: number) {
    this.sliderStartValue = value;
    // this.searchServers();
  }
  onSliderEndValueChanged(value: number) {
    this.sliderEndValue = value;
    // this.searchServers();
  }
  onHddTypeValueChanged(value: string) {
    this.hddType = value;
    // this.searchServers();
  }

  onRamValueChanged(value: string[]) {
    this.ram = value;
    // this.searchServers();
  }
  onLocationValueChanged(value: string) {
    this.location = value;
    // this.searchServers();
  }

  onSearchButtonClicked() {
    this.searchServers();
  }

  onCompareButtonClicked() {

    let numberOfSelectedServers = this.selection.selected.length;
    console.log(numberOfSelectedServers);

    if (numberOfSelectedServers > 4) {
      alert('Please select maximum 4 items to compare');
      return;

    }

    if (!numberOfSelectedServers || numberOfSelectedServers < 2) {
      alert('Please select atleast 2 items to compare');
      return
    }

    const queryParams = { ids: 'value1' };
    let ids = this.selection.selected.map(a => a.id);
    this.router.navigate(['/server-compare'], { queryParams: { serverIds: ids } });

  }

  searchServers() {
    let filter: ServerFilter = {
      storage: [this.sliderStartValue.toString(), this.sliderEndValue.toString()],
      ram: this.ram,
      hddType: this.hddType,
      hdd: '',
      location: this.location
    }
    this.serverService.searchServers(filter)
      .subscribe(servers => this.servers = servers);
  }
}
